'use client';
import {useEffect, useState} from 'react';
import {useSession} from 'next-auth/react';
import {useRouter} from 'next/navigation';
import Profile from '@components/Profile';

const MyProfile = () => {
    const router = useRouter();
    const {data: session} = useSession();
    const [posts, setPosts] = useState([]);
    useEffect(() => {
        const fetchPosts = async () => {
            console.log('fetchPosts')
            const response = await fetch(`/api/users/${session?.user.id}/posts`);
            const data = await response.json();
            setPosts(data);
        }
        fetchPosts();
    }, []);
    const handleEdit = (post) => {
        router.push(`/update-prompt?id=${post._id}`)
    }
    const handleDelete = async (post) => {
        const hasConfirmed = confirm('Are you sure you want to delete this item ?')
        if (!hasConfirmed) return false;

        try{
            const response = await fetch(`/api/prompt/${post._id.toString()}`,{
                method: 'DELETE'
            });
            const filtredPosts = posts.filter((p) => p._id !== post._id);
            setPosts(filtredPosts);

        } catch (e){
            console.log(e)
        }


    }
    return (
        <Profile
            name="My"
            desc="Welcome to your profile page"
            data={posts}
            handleEdit={handleEdit}
            handleDelete={handleDelete}
        />
    );
}

export default MyProfile;