import {connectToDB} from "@utils/database";
import Prompt from "@models/prompt";
export const GET = async (requsest) => {
    try {
        await connectToDB();
        const prompts = await Prompt.find({}).populate('creator');
        return new Response(JSON.stringify(prompts), {status: 200});
    } catch (e){
        console.log(e)
        return new Response('Failed to get prompts list', {status: 500})
    }
}