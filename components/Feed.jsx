'use client';
import {useState, useEffect} from 'react';
import PromptCard from "@components/PromptCard";


const PromptCardList = ({data, handleTagClick}) => {
    return (
        <div className="mt-16 prompt_layout">
            {data.map((post) => (
                <PromptCard key={post._id} post={post} handleTagClick={handleTagClick}/>
            ))}
        </div>
    );
}
const Feed = () => {
    const [searchTerm, setSearchTerm] = useState('');
    const [posts, setPosts] = useState([]);
    const [filtredPosts, setFiltredPosts] = useState([]);
    const handleSearchChange = (e) => {
        let term = e.target.value;
        console.log(term)
        const filtredData = posts.filter((p) => p.prompt.toLowerCase().includes(term.toLowerCase()) || p.tag.toLowerCase().includes(term.toLowerCase()) || p.creator.username.toLowerCase().includes(term.toLowerCase()))
        setFiltredPosts((term === '') ? posts : filtredData);


    }
    const fetchPosts = async () => {
        console.log('fetchPosts')
        const response = await fetch('/api/prompt');
        const data = await response.json();
        setPosts(data);
        const filtredData = posts.filter((p) => p.prompt.toLowerCase().includes(searchTerm.toLowerCase()) || p.tag.toLowerCase().includes(searchTerm.toLowerCase()) || p.creator.username.toLowerCase().includes(searchTerm.toLowerCase()))
        setFiltredPosts((searchTerm === '') ? posts : filtredData);
    }
    useEffect(() => {
        fetchPosts();

    }, [searchTerm]);


    return (
        <section className="feed">
            <form className="relative w-full flex-center">
                <input
                    type="text"
                    placeholder="Search term"

                    onChange={(e) => setSearchTerm(e.target.value) }
                    required
                    className="search_input"/>
            </form>
            <PromptCardList data={filtredPosts} handleTagClick={() => {
            }}/>
        </section>
    );
}

export default Feed;